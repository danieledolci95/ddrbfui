CSS = (
"""

QWidget#Yumi
{
    color: #b1b1b1;
    background-color: #252525;
}

QWidget::focus
{
    border: 2px solid QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff3399, stop: 1 #f71184);
}

QWidget:item:hover
{
    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff3399, stop: 1 #f71184);
}

QWidget:item:selected
{
    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff3399, stop: 1 #f71184);
}

QPushButton#pink
{
    background-color: #E62E8A; 

}

QPushButton#pink:hover
{
    background-color: #E64596;

}

QPushButton#pink:pressed
{
    background-color: #E64596;
    border: black 2px
}

QPushButton#pink:hover:!pressed
{
    background-color: #E64596;
    border: black 2px
}

QPushButton#red
{
    background-color: #DA3E3E; 

}

QPushButton#red:hover
{
    background-color: #D86666;

}

QPushButton#red:pressed
{
    background-color: #D86666;
    border: black 2px
}

QPushButton#red:hover:!pressed
{
    background-color: #D86666;
    border: black 2px
}

QPushButton#green
{
    background-color: #33BC33; 

}

QPushButton#green:hover
{
    background-color: #76D064;

}

QPushButton#green:pressed
{
    background-color: #76D064;
    border: black 2px
}

QPushButton#green:hover:!pressed
{
    background-color: #76D064;
    border: black 2px
}

QScrollArea, QScrollArea::focus
{
    border: 0px;
}

""")

# QTreeView::branch:!has-children:adjoins-item:hover, QTreeView::branch:!has-children:adjoins-item:selected{
#         background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff3399, stop: 1 #f71184);
# }

# QTreeView::branch:has-children:!has-siblings:closed {
#     image: url('C:/Users/Ives/Documents/workspace/MayaSandbox/release_tool/sanity_checks/images/plus.png');
# }

# QTreeView::branch:has-children:!has-siblings:open {
#     image: url('C:/Users/Ives/Documents/workspace/MayaSandbox/release_tool/sanity_checks/images/minus.png');
# }

# QTreeView::branch:open:has-children:!has-siblings:hover, QTreeView::branch:open:has-children:!has-siblings:selected {
#         background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff3399, stop: 1 #f71184);
# }

# QTreeView::branch:closed:has-children:!has-siblings:hover, QTreeView::branch:closed:has-children:!has-siblings:selected {
#         background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff3399, stop: 1 #f71184);
# }

# QTreeView::branch:!has-siblings:!adjoins-item:hover, QTreeView::branch:!has-siblings:!adjoins-item:selected {
#         background: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ff3399, stop: 1 #f71184);
# }
# CSS = (
# """
# QPushButton {
# 	background-color: QLinearGradient(
#         x1:1, y1:0,
#         x2:0, y2:0,
#         stop:1 #ff3399,
#         stop:0.94 #ff3399,
#         stop:0.939 #343434,
#         stop:0.4 #343434/*,
#         stop:0.2 #ff3399,
#         stop:0.1 #ff3399*/
#     );
# }

# QPushButton::hover{
# 	background-color: QLinearGradient(
#         x1:1, y1:0,
#         x2:0, y2:0,
#         stop:1 #ff3399,
#         stop:0.86 #ff3399,
#         stop:0.859 #343434,
#         stop:0.4 #343434/*,
#         stop:0.2 #ff3399,
#         stop:0.1 #ff3399*/
#     );
#     padding: 0px 0px 0px 10px;
# }

# QPushButton #Login, #Release, #Gather {
# 	background-color: #343434!important;
# 	border: 0px solid #ff3399!important;
# }

# QPushButton::hover #Login, #Release, #Gather{
# 	background-color: #343434!important;
#     border: 1px solid #ff3399!important;
#     padding: 0px 0px 0px 0px;
# }

# QListWidget {
# 	alternate-background-color: #1a1a1a;
# 	background-color: #262626;
# 	paint-alternating-row-colors-for-empty-area: 1;
# }

# QMenuBar::item::selected{
# 	background-color: #ff3399;
# }

# QLineEdit::focus {
# 	border: 1px solid #ff3399;
# }

# QListWidget::focus {
# 	border: 1px solid #ff3399;
# }

# QListWidget::item::selected {
# 	background-color: #ff3399;

# }

# QTextEdit::focus {
# 	border: 1px solid #ff3399;
# }

# QComboBox QAbstractItemView {
#     selection-background-color: #ff3399;
# }

# """
# )


# PURPLE = "#ff3399"
