# -*- coding: utf-8 -*-

from maya import cmds
from Qt import QtWidgets, QtCore, QtGui
import ddRbfUi.CSS as CSS

reload(CSS)

class BaseWidget(QtWidgets.QWidget):

    def __init__(self, parent=None, object_name=None, window_name=None):
        super(BaseWidget, self).__init__(parent=parent)
        self._parent = parent
        self._object_name = object_name
        self._window_name = window_name
        self._init_interface()
        self.setStyleSheet(CSS.CSS)

    def _init_interface(self):
        """Initialize Interface"""
        if self._object_name: 
                self.setObjectName(self._object_name)

        if self._window_name: 
                self.setWindowTitle(self._window_name)
        
        if self._parent:
            parent_pos = self.parent().pos()
            parent_width = self.parent().width()
            parent_height = self.parent().height()

            parent_center_x = parent_pos.x() + parent_width / 2
            parent_center_y = parent_pos.y() + parent_height / 2

            self.move(QtCore.QPoint(parent_center_x - self.width() / 2,
                                    parent_center_y - self.height() / 2))

    @staticmethod
    def HLine():
        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.HLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)
        
        return line
    
    @staticmethod
    def VLine():
        line = QtWidgets.QFrame()
        line.setFrameShape(QtWidgets.QFrame.VLine)
        line.setFrameShadow(QtWidgets.QFrame.Sunken)

        return line







