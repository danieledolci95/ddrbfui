# -*- coding: utf-8 -*-
MAIN_OBJECT_NAME = "ddRbfUi"
MAIN_WINDOW_NAME = "ddRbfUi v1.0"

# Menu
M_FILE = "File"
M_EDIT = "Edit"
M_SETTINGS = "Settings"
M_HELP = "?"

# RBF Buttons
B_SELECT = "Select"
B_TOGGLE = "Toggle"
B_RBFDELETE = "Delete"
B_MIRROR = "Mirror"
B_RBFCREATE = "Create Rbf"
B_QRBFCREATE = "Create qRbf"
B_UPDATE = "Update"
B_CREATE = "Create"
B_DELETE = "Delete-"
B_ADD = "Add+"
B_SSP = "Select Sample Pos"
B_SSV = "Select Sample Value"

# Labels 
L_DRIVER = "Driver:"
L_DRIVEN = "Driven:"
L_NSAMPLES = "Samples Amount:"
L_SSAMPLES = "Sample size:"
L_INTERPTYPE = "Interpolation:"
L_EPSILON = u"Epsilon (ε):"
L_RBFTYPE = "Rbf type:"
L_NAME = "Name:"
L_USE_XFORM = "Use xform:"

# Interpolation ComboBox
CB_GAUSS = "Gaussian"
CB_MULTIQUADRATIC = "MultiQuadratic"
CB_INVERSEQUADRATIC = "InverseQuadratic"
CB_INVERSEMULTIQUADRATIC = "InverseMultiQuadratic"
CB_POLYHARMONIC = "PolyHarmonic"
CB_THINPLATE = "ThinPlate"

# Rbf type ComboBox
CB_RBF = "ddRbf"
CB_QRBF = "ddQRbf"

# CSS
CSS_PINK = "pink"
CSS_RED = "red"

# ERRORS
E_WINDOW_NOT_FOUND = 'Could not find MayaWindow instance'

#TABS
T_SAMPLES = "Samples"
T_OUTPUTS = "Outputs"
