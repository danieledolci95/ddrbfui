from maya import cmds
from Qt import QtWidgets, QtCore, QtGui
from ddRbfUi import const


class Controller(QtCore.QObject):
    view = None
    model = None

    def __init__(self, view, model):

        super(Controller, self).__init__()

        self.view = view
        self.model = model

        # get subwidgets from view
        self._settings_widget = self.view.get_settings_widget()
        self._samples_widget = self.view.get_samples_widget()
        self._rbf_widget = self.view.get_creator_widget()

        # connect all the signals
        self._connect_signals()

        # get data from rbf solver in scene
        self.model.fetch_data_from_scene()

    def _connect_signals(self):

    	# View signals
    	self.view.selection_changed_signal[str].connect(self.update_rbf_data)
    	self.view.select_signal[str].connect(self.select_rbf)
    	self.view.toggle_signal[str].connect(self.toggle_rbf)
    	self.view.delete_signal[str].connect(self.delete_rbf)


        self._settings_widget.update_signal[dict].connect(self.update_rbf_attributes)

        self._samples_widget.sample_cell_updated_signal[list].connect(self.update_rbf_samples)
        self._samples_widget.add_sample_pressed_signal.connect(self.add_sample)
        self._samples_widget.delete_sample_pressed_signal[int].connect(self.delete_sample)
        self._samples_widget.select_samplep_pressed_signal[int].connect(self.select_sample_pos)
        self._samples_widget.select_samplev_pressed_signal[int].connect(self.select_sample_value)

        self._rbf_widget.create_signal[dict].connect(self.create_rbf)

    	# Model signals
    	self.model.scene_data_changed_signal[list].connect(self.update_rbf_list)
        self.model.rbf_changed_signal[dict].connect(self.update_view)

    #From view
    @QtCore.Slot(str)
    def update_rbf_data(self, the_rbf):
        self.model.select_rbf(the_rbf)

    @QtCore.Slot(str)
    def select_rbf(self, the_rbf):
        self.model.select_rbf_node()

    @QtCore.Slot(str)
    def toggle_rbf(self, the_rbf):
        self.model.toggle_visibility()

    @QtCore.Slot(str)
    def delete_rbf(self, the_rbf):
        self.model.delete_rbf_node()

    @QtCore.Slot(list)
    def update_rbf_attributes(self, data):
        self.model.update_rbf_attributes(data)

    @QtCore.Slot(list)
    def update_rbf_samples(self, data):
        self.model.update_sample_values(data)

    @QtCore.Slot(str)
    def add_sample(self):
        self.model.add_sample()

    @QtCore.Slot(str)
    def delete_sample(self, index):
        self.model.delete_sample(index)
   
    @QtCore.Slot(str)
    def select_sample_pos(self, index):
        self.model.select_sample_pos(index)

    @QtCore.Slot(str)
    def select_sample_value(self, index):
        self.model.select_sample_value(index)

    @QtCore.Slot(str)
    def create_rbf(self, the_settings):
        self.model.create_rbf_node(the_settings)

    # From model
    @QtCore.Slot(str)
    def update_rbf_list(self, the_list):
    	self.view.populate_rbf_list(the_list)

    @QtCore.Slot(str)
    def update_view(self, data):
        self._settings_widget.populate_settings(data)
        self._samples_widget.populate_samples(data)

        