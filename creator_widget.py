# -*- coding: utf-8 -*-
from maya import cmds
from Qt import QtWidgets, QtCore, QtGui
from ddRbfUi import const
from ddRbfUi import base_widget
import ddRbfUi.CSS as CSS

class CreatorWidget(base_widget.BaseWidget):
    create_signal = QtCore.Signal(list)

    def __init__(self, parent=None):
        super(CreatorWidget, self).__init__(parent=parent)

    def _init_interface(self):
        """Initialize Interface"""
        
        super(CreatorWidget, self)._init_interface()

        # Create main layout
        settings_layout = QtWidgets.QGridLayout(self)
        settings_layout.setSpacing(1)
        settings_layout.setMargin(1)
        settings_layout.setAlignment(QtCore.Qt.AlignTop)

        # Create and add the solver type row
        solver_label = QtWidgets.QLabel(const.L_RBFTYPE)
        self._solver_line = QtWidgets.QComboBox()
        self._solver_line.addItem(const.CB_RBF)
        self._solver_line.addItem(const.CB_QRBF)
        
        settings_layout.addWidget(solver_label, 0, 0)
        settings_layout.addWidget(self._solver_line, 0, 1)

        # Create and add the name row
        name_label = QtWidgets.QLabel(const.L_NAME)
        self._name_line = QtWidgets.QLineEdit()

        settings_layout.addWidget(name_label, 1, 0)
        settings_layout.addWidget(self._name_line, 1, 1)

        # Create and add the driver row
        driver_label = QtWidgets.QLabel(const.L_DRIVER)
        self._driver_line = QtWidgets.QLineEdit()

        settings_layout.addWidget(driver_label, 2, 0)
        settings_layout.addWidget(self._driver_line, 2, 1)

        # Create and add the samples amount row
        samples_label = QtWidgets.QLabel(const.L_NSAMPLES)
        self._samples_line = QtWidgets.QLineEdit("0")
        self._samples_line.setValidator(QtGui.QIntValidator(0, 1000000))

        settings_layout.addWidget(samples_label, 3, 0)
        settings_layout.addWidget(self._samples_line, 3, 1)

        # Create and add the use xform row
        self._use_xform_label = QtWidgets.QLabel(const.L_USE_XFORM)
        self._use_xform = QtWidgets.QCheckBox()

        settings_layout.addWidget(self._use_xform_label, 4, 0)
        settings_layout.addWidget(self._use_xform, 4, 1)

        # Create and add the xform group row
        self._xform_group = QtWidgets.QGroupBox()
        self._xform_group.setVisible(False)
        self._group_layout = QtWidgets.QHBoxLayout(self._xform_group)
        self._translate_group = QtWidgets.QCheckBox("translate")
        self._rotate_group = QtWidgets.QCheckBox("rotate")
        self._scale_group = QtWidgets.QCheckBox("scale")

        self._translate_group.setChecked(True)
        self._rotate_group.setChecked(True)
        self._scale_group.setChecked(True)

        self._group_layout.addWidget(self._translate_group)
        self._group_layout.addWidget(self._rotate_group)
        self._group_layout.addWidget(self._scale_group)

        settings_layout.addWidget(self._xform_group, 5, 0, 1, 2)

        # Create and add the sample size row
        self._samplessize_label = QtWidgets.QLabel(const.L_SSAMPLES)
        self._samplessize_line = QtWidgets.QLineEdit("0")
        self._samplessize_line.setValidator(QtGui.QIntValidator(0, 1000000))

        settings_layout.addWidget(self._samplessize_label, 5, 0)
        settings_layout.addWidget(self._samplessize_line, 5, 1)

        # Create and add the kernel row
        interpolation_label = QtWidgets.QLabel(const.L_INTERPTYPE)
        self._interpolation_line = QtWidgets.QComboBox()

        self._interpolation_line.addItem(const.CB_GAUSS)
        self._interpolation_line.addItem(const.CB_MULTIQUADRATIC)
        self._interpolation_line.addItem(const.CB_INVERSEQUADRATIC)
        self._interpolation_line.addItem(const.CB_INVERSEMULTIQUADRATIC)
        self._interpolation_line.addItem(const.CB_POLYHARMONIC)
        self._interpolation_line.addItem(const.CB_THINPLATE)

        """
        d = distance in R^n; r = d^2, k=round(ε)
        Gaussian:              e^(-ε*r)
        MultiQuadratic:        (1+(ε*d)^2)^1/2
        InverseQuadratic:      1/(ε*d)^2
        InverseMultiQuadratic: 1/(1+(ε*d)^2)^1/2
                               d^k         with k=1,3,5,...,2n+1
        PolyHarmonic:        {                                  }
                               log(d)*d^k  with k=2,4,6,...,2n
        ThinPlate:             log(d)*d^2
        """
        settings_layout.addWidget(interpolation_label, 6, 0)
        settings_layout.addWidget(self._interpolation_line, 6, 1)

        # Create and add the epsilon row
        epsilon_label = QtWidgets.QLabel(const.L_EPSILON)
        self._epsilon_line = QtWidgets.QLineEdit("0")
        self._epsilon_line.setValidator(QtGui.QDoubleValidator(0, 1000000, 2))

        settings_layout.addWidget(epsilon_label, 7, 0)
        settings_layout.addWidget(self._epsilon_line, 7, 1)

        # Create and add the create button
        self._create_button = QtWidgets.QPushButton(const.B_CREATE)
        self._create_button.setObjectName(const.CSS_PINK)
        settings_layout.addWidget(self._create_button, 8, 0, 1, 2)
        
        # Make connections
        self._make_connections()

    def _make_connections(self):
        self._create_button.clicked.connect(self.emit_create_settings)
        self._solver_line.currentIndexChanged.connect(self._update_samplesize)
        self._use_xform.stateChanged.connect(self._use_xform_changed)

    def emit_create_settings(self):
        data = {"d": self._driver_line.text(), 
                "n": self._name_line.text(), 
                "sa": int(self._samples_line.text()), 
                "ss": int(self._samplessize_line.text()), 
                "k": self._interpolation_line.currentText(), 
                "ep": float(self._epsilon_line.text()),
                "solver": self._solver_line.currentText(),
                "usx": self._use_xform.isChecked(),
                "ust": self._translate_group.isChecked(),
                "usr": self._rotate_group.isChecked(),
                "uss": self._scale_group.isChecked(),}
        self.create_signal.emit(data)
        self.reset()

    def _update_samplesize(self):
        if self._solver_line.currentText() == const.CB_RBF:
            self._samplessize_line.setEnabled(True)
            self._samplessize_line.setVisible(True)
            self._samplessize_label.setVisible(True)
            self._use_xform.setEnabled(True)
            self._samplessize_line.setText("0")
            self._use_xform_label.setEnabled(True)
        else:
            self._samplessize_line.setEnabled(False)
            self._samplessize_label.setEnabled(False)
            self._use_xform.setEnabled(False)
            self._use_xform.setChecked(False)
            self._use_xform_label.setEnabled(False)
            self._samplessize_line.setText("4")

    def _use_xform_changed(self):
        if self._use_xform.isChecked():
            self._samplessize_line.setEnabled(False)
            self._samplessize_line.setVisible(False)
            self._samplessize_label.setVisible(False)
            self._xform_group.setEnabled(True)
            self._xform_group.setVisible(True)
        else:
            self._samplessize_line.setEnabled(True)
            self._samplessize_line.setVisible(True)
            self._samplessize_label.setVisible(True)
            self._xform_group.setEnabled(False)
            self._xform_group.setVisible(False)

    def reset(self):
        self._solver_line.setCurrentIndex(0)
        self._name_line.setText("")
        self._driven_line.setText("")
        self._samples_line.setText("")
        self._use_xform.setChecked(False)
        self._translate_group.setChecked(True)
        self._rotate_group.setChecked(True)
        self._scale_group.setChecked(True)
        self._samplessize_line.setText("")
        self._interpolation_line.setCurrentIndex(0)
        self._epsilon_line.setText("")



        