from Qt import QtWidgets, QtCore, QtGui
from ddRbfUi import view as ddRbfView
from ddRbfUi import model as ddRbfModel
from ddRbfUi import controller as ddRbfController
reload(ddRbfView)
reload(ddRbfModel)
def get_maya_main_window():
    """
    Returns the QWidget-object of the main maya application.
    """
    for obj in QtWidgets.qApp.topLevelWidgets():
        if obj.objectName() == 'MayaWindow':
            return obj
    raise RuntimeError(const.E_WINDOW_NOT_FOUND) 

def main():
    parent = get_maya_main_window()
    view = ddRbfView.ddRbfView(parent)
    view.show()

    model = ddRbfModel.Model()

    controller = ddRbfController.Controller(view, model)

    return controller

if __name__ == "__main__":
    view = main()

