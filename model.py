from maya import cmds
from Qt import QtWidgets, QtCore, QtGui
from ddRbfUi import rbf_model
from ddRbfUi import const
import json

class Model(QtCore.QObject):
    scene_data_changed_signal = QtCore.Signal(list)
    rbf_changed_signal = QtCore.Signal(list)
    samples_changed_signal = QtCore.Signal(dict)

    def __init__(self):

        super(Model, self).__init__()

    	self._rbf_list = []

        # instanciate the RbfModel to represent 
        # the data of a single solver.
        self._rbf = rbf_model.RbfModel()

    def fetch_data_from_scene(self):
        """
        Get a list of all the rbf nodes.
        """
    	data = cmds.ls(type="ddRbf")+cmds.ls(type="ddQRbf")
    	if self._rbf_list != data:
    		self._rbf_list = data
    		self.scene_data_changed_signal.emit(self._rbf_list)

    def select_rbf(self, value):
        """
        Change the rbf whose data are represented
        by the rbf model.
        """
        self._rbf.node = value
        the_settings = self._rbf.fetch()
        self.rbf_changed_signal.emit(the_settings)

    def update_rbf_attributes(self, data):
        """
        Update come settings on rbf according
        to data coming from the View.
        """
        self._rbf.rename_node(data["n"])
        self._rbf.driver = data["d"]
        self._rbf.kernel = data["k"]
        self._rbf.epsilon = data["ep"]
        the_settings = self._rbf.fetch()
        self.rbf_changed_signal.emit(the_settings)

    def update_sample_values(self, data):
        """
        Update the value of the samples according 
        to data coming from the View. 
        """
        self._rbf.set_sample_values(data)

        the_settings = self._rbf.fetch()
        self.rbf_changed_signal.emit(the_settings)

    def add_sample(self):
        """
        Add sample to Rbf Solver. 
        """
        self._rbf.add_sample()

        the_settings = self._rbf.fetch()
        self.rbf_changed_signal.emit(the_settings)

    def select_rbf_node(self):
        """
        Select the rbf node.
        """
        self._rbf.select()

    def select_sample_pos(self, index):
        """
        Select the sample Position object at 
        the given index.
        """
        self._rbf.select_samplepos_at(index)

    def select_sample_value(self, index):
        """
        Select the sample Value object at 
        the given index.
        """
        self._rbf.select_samplevalue_at(index)

    def delete_rbf_node(self):
        """
        Delete the current represented rbf node.
        """
        cmds.delete(self._rbf.node)
        for x in self._rbf._dcms:
            try:
                cmds.delete(x[0])
            except:
                pass
        self.fetch_data_from_scene()

    def create_rbf_node(self, the_settings):
        """
        Create rbf node according to the settings
        specified in the View.
        """
        if the_settings["solver"] == "ddRbf":
            cmds.ddRbf(n=the_settings["n"],
                       d=the_settings["d"],
                       sa=the_settings["sa"],
                       ss=the_settings["ss"],
                       k=the_settings["k"],
                       ep=the_settings["ep"],
                       usx=the_settings["usx"],
                       ust=the_settings["ust"],
                       usr=the_settings["usr"],
                       uss=the_settings["uss"])
        else:
            cmds.ddQRbf(n=the_settings["n"],
                       d=the_settings["d"],
                       sa=the_settings["sa"],
                       k=the_settings["k"],
                       ep=the_settings["ep"])

        self.fetch_data_from_scene()

    def delete_sample(self, index):
        """
        Delete samle Position and Value
        at the given index.
        """
        self._rbf.delete_sample(index)
        the_settings = self._rbf.fetch()
        self.rbf_changed_signal.emit(the_settings)

    def toggle_visibility(self):
        """
        Toggle visibility of samples.
        """
        self._rbf.toggle()
