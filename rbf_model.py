from maya import cmds
from Qt import QtWidgets, QtCore, QtGui
from ddRbfUi import const
import json

class RbfModel(object):

    _KERNELS = ["gaussian",
                "multiquadratic",
                "inversequadratic",
                "inversemultiquadratic",
                "polyharmonic",
                "thinplate"]

    _TYPES = ["ddRbf",
              "ddQRbf"]

    _AXIS = ["X", "Y", "Z", "W"]

    def __init__(self):

        super(RbfModel, self).__init__()

        self._rbf = None
        self._solver_type = None
    	self._driver = None
        self._outputs = None
        self._samples_pos = []
        self._samples_value = []
        self._samples_amount = 0
        self._samples_size = 0
        self._kernel = 0
        self._epsilon = 0
        
        self._dcms = []

        self._command = None

    @property
    def node(self):
        return self._rbf

    @node.setter
    def node(self, value):
        self._solver_type = cmds.nodeType(value) if cmds.nodeType(value) in self._TYPES else None

        # Reset lists
        self._samples_pos = []
        self._samples_value = []
        self._dcms = []

        # If solver is defined
        if self._solver_type:
            # Assign to self._command either 
            # cmds.ddRbf or cmds.ddQRbf
            self._command = cmds.ddRbf if self._solver_type == self._TYPES[0] else cmds.ddQRbf
            self._rbf = value

            # Perform queries valid for both solvers
            self._driver = self._command(n=self._rbf, q=True, d=True)
            self._samples_amount = self._command(n=self._rbf, q=True, sa=True)
            self._samples_size = self._command(n=self._rbf, q=True, ss=True) or 4

            self._samples_pos = [x for i in range(self._samples_amount) 
                                 for x in self._command(n=self._rbf, q=True, sp=True, sai=i)]

            # Looping over the samples
            for i in range(self._samples_amount):

                # Get the sample value at index i
                command_result = self._command(n=self._rbf, q=True, sv=True, sai=i)
                to_append = []

                # If the solver is ddRbf
                if self._solver_type == self._TYPES[0]:

                    # For input the the result of the query
                    for x in command_result:

                        # We split the name of the attribute and 
                        # take the name of the node
                        node = x.split(".")[0]

                        try:
                            # Skipping the unitconversion nodes
                            node_type = cmds.nodeType(node)
                            if node_type == "unitConversion":
                                x = cmds.connectionInfo("{0}.input".format(node), sfd=True)
                            to_append.append(x)
                        except:
                            pass

                    self._samples_value.append(to_append)
                    
                else: # if solver is ddQRbf

                    # No need to loop over because
                    # ddQRbf only has 1 input for each value
                    node = command_result[0].split(".")[0]
                    node_type = cmds.nodeType(node)

                    # Skipping the decompose matrix nodes
                    if node_type == "decomposeMatrix":
                        self._dcms.append([node])
                        x = cmds.connectionInfo("{0}.inputMatrix".format(node), sfd=True)

                    self._samples_value.append([x])  
                          
            self._outputs = [self._command(n=self._rbf, q=True, o=True, oai=i)
                             for i in range(self._samples_size)]

            self._kernel = self._command(n=self._rbf, q=True, k=True)
            

            self._epsilon = 1

    @property
    def solver_type(self):
        return self._solver_type

    @property
    def driver(self):
        return self._driver

    @driver.setter
    def driver(self, value):
        cmds.connectAttr(value, "{0}.inputMatrix".format(self._rbf), f=True)
        self._driver = value

    @property
    def outputs(self):
        return self._outputs

    @outputs.setter
    def outputs(self, value):
        self.disconnect_outputs()
        for i in range(len(value)):
            for destination in value[i]:
                cmds.connectAttr("{0}.outValue[{1}]".format(self._rbf, i),
                                 destination,
                                 f=True)
        self._outputs = value

    @property
    def kernel(self):
        return self._kernel

    @kernel.setter
    def kernel(self, value):
        cmds.setAttr("{0}.rbfKernel".format(self._rbf), self._KERNELS.index(value.lower()))
        self._kernel = value

    @property
    def epsilon(self):
        return self._epsilon

    @epsilon.setter
    def epsilon(self, value):      
        cmds.setAttr("{0}.epsilon".format(self._rbf), self._epsilon)
        self._epsilon = value

    @property
    def samples_pos(self):
        return self._samples_pos

    @samples_pos.setter
    def samples_pos(self, value):  
        for i, pos in enumerate(value):
            cmds.connectAttr(pos,
                             "{0}.samples[{1}].samplePosition".format(self._rbf, i),
                             f=True)
        self._samples_pos = value
        self._samples_amount = len(self._samples_pos)

    @property
    def samples_value(self):
        return self._samples_value

    @samples_value.setter
    def samples_value(self, value):   
        self._dcms = []
        for i in range(len(value)):
            if self._solver_type == self._TYPES[0]:
                for k in range(len(value[i])):         
                        cmds.connectAttr(value[i][k],
                                         "{0}.samples[{1}].sampleValue[{2}]".format(self._rbf, i, k))
            else:
                dcm = cmds.createNode("decomposeMatrix", n="{0}_DCM".format(value[i][0].split(".")[0]))
                self._dcms.append([dcm])
                cmds.connectAttr(value[i][0],
                                 "{0}.inputMatrix".format(dcm))
                for k in range(4):
                    cmds.connectAttr("{0}.outputQuat{1}".format(dcm, self._AXIS[k]),
                                     "{0}.samples[{1}].sampleValue[{2}]".format(self._rbf, i, k))
        self._samples_value = value

    @property
    def samples_amount(self):
        return self._samples_amount

    @property
    def samples_size(self):
        return self._samples_size

    def fetch(self):
        if self._solver_type == self._TYPES[0]:
            sv = {"strings": self._samples_value,

                  "floats": [[cmds.getAttr(x) for x in sample]
                              for sample in self._samples_value],

                  "attributes": [self._samples_value[i][0].split(".")[0] 
                             for i in range(self._samples_amount)]}

            isSampleEditable = True

        else:
            quat_attributes = [cmds.ddQRbf(n=self._rbf, q=True, sv=True, sai=i)
                               for i in range(self._samples_amount)]

            sv = {"strings": quat_attributes,

                  "floats": [[cmds.getAttr(x) for x in quat] 
                           for quat in quat_attributes],

                  "attributes": [self._samples_value[i][0].split(".")[0] 
                             for i in range(self._samples_amount)]}
                             
            isSampleEditable = False

        data = {"n": self._rbf,
                "d": self._driver,
                "sa": self._samples_amount,
                "ss": self._samples_size,
                "sp": {"strings": self._samples_pos,
                       "floats": [cmds.xform(x.split(".")[0], q=True, m=True)
                       for x in self._samples_pos]},
                "sv": sv,
                "o": self._outputs,
                "k": self._KERNELS.index(self._kernel.lower()),
                "ep": self._epsilon,
                "isSampleEditable": isSampleEditable}
        return data

    def set_sample_values(self, data):
        for sample_value, data_value in zip(self._samples_value, data):
            for attr, value in zip(sample_value, data_value):
                cmds.setAttr(attr, value)

    def select(self):
        cmds.select(cl=True)
        cmds.select(self._rbf)

    def select_driver(self):
        cmds.select(cl=True)
        cmds.select(self._driver)

    def select_outputs_at(self, index):
        cmds.select(cl=True)
        cmds.select([x.split(".")[0] for x in self._outputs[index]])

    def select_samplepos_at(self, index):
        cmds.select(cl=True)
        cmds.select(self._samples_pos[index].split(".")[0])

    def select_samplevalue_at(self, index):
        cmds.select(cl=True)
        cmds.select([x.split(".")[0] for x in self._samples_value[index]])

    def delete_sample(self, index):
        self.disconnect_samples()

        cmds.delete(self._samples_pos[index].split(".")[0])
        cmds.delete(self._samples_value[index][0].split(".")[0])

        del(self._samples_pos[index])
        del(self._samples_value[index])
        if self._dcms:
            del(self._dcms[index])

        self.samples_pos = self._samples_pos
        self.samples_value = self._samples_value

    def disconnect_samples(self):
        for i in range(self._samples_amount):
            cmds.disconnectAttr(self._samples_pos[i], 
                                "{0}.samples[{1}].samplePosition".format(self._rbf, i))
            if self._solver_type == self._TYPES[0]:    
                for k in range(len(self._samples_value[i])):
                    cmds.disconnectAttr(self._samples_value[i][k], 
                                        "{0}.samples[{1}].sampleValue[{2}]".format(self._rbf, i, k)) 
            else:
                cmds.delete(self._dcms[i][0])

    def disconnect_outputs(self):
        for i in range(len(self._outputs)):
            for k in range(len(self._outputs[i])):
                cmds.disconnectAttr("{0}.outValue[{1}]".format(self._rbf, i), 
                                    self._outputs[i][k])

    def set_value_of_sample(self, sample_index, value_index, value):
        cmds.setAttr(self._samples_value[sample_index][value_index], value)


    def add_sample(self, sample=None):
        if sample is None:
            loc = cmds.spaceLocator(n="{0}_samplePos{1}".format(self._rbf, self._samples_amount+1))[0]
            sphere = cmds.polySphere(n="{0}_sampleValue{1}".format(self._rbf, self._samples_amount+1))[0]
        else:
            loc, sphere = sample

        self.disconnect_samples()

        plugs = [x.split(".")[1] for x in self._samples_value[0]]
        parent = cmds.listRelatives(self._samples_value[0].split(".")[0], p=True)[0]

        cmds.parent(loc, parent)
        cmds.parent(sphere, parent)

        value_plugs = []
        for plug in plugs:
            if not cmds.objExists("{0}.{1}".format(sphere, plug)):
                cmds.addAttr(sphere, ln=plug, at="double", k=True)
            value_plugs.append("{0}.{1}".format(sphere, plug))

        self._samples_pos.append("{0}.matrix".format(loc))
        self._samples_value.append(value_plugs)
        
        self.samples_pos = self._samples_pos
        self.samples_value = self._samples_value

    def toggle(self):

        for pos, value in zip(self._samples_pos, self._samples_value):
            cmds.setAttr("{0}.v".format(pos.split(".")[0]), 
                         1-cmds.getAttr("{0}.v".format(pos.split(".")[0])))
            for v in value:
                cmds.setAttr("{0}.v".format(v.split(".")[0]), 
                             1-cmds.getAttr("{0}.v".format(v.split(".")[0])))

    def rename_node(self, the_name):
        cmds.rename(self._rbf, the_name)
        self.node = the_name