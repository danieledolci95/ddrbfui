# -*- coding: utf-8 -*-
from maya import cmds
from Qt import QtWidgets, QtCore, QtGui
from ddRbfUi import const
from ddRbfUi import base_widget
import ddRbfUi.CSS as CSS

class SamplesWidget(base_widget.BaseWidget):
    sample_cell_updated_signal = QtCore.Signal(list)
    add_sample_pressed_signal = QtCore.Signal()
    delete_sample_pressed_signal = QtCore.Signal(int)
    select_samplep_pressed_signal = QtCore.Signal(int)
    select_samplev_pressed_signal = QtCore.Signal(int)

    def __init__(self, parent=None):
        self._samples = []
        self._separators = []

        self._tables = []

        self._delete_buttons = []
        self._selectp_buttons = []
        self._selectv_buttons = []

        super(SamplesWidget, self).__init__(parent=parent)

    def _init_interface(self):
        """Initialize Interface"""
        
        super(SamplesWidget, self)._init_interface()

        # Create main layout
        self._samples_layout = QtWidgets.QVBoxLayout(self)
        self._samples_layout.setAlignment(QtCore.Qt.AlignTop)

        # Create and add "add" button
        self._add_sample = QtWidgets.QPushButton(const.B_ADD)
        self._add_sample.setEnabled(False)
        self._add_sample.setMaximumWidth(200)
        self._samples_layout.addWidget(self._add_sample)
        self._samples_layout.setAlignment(self._add_sample, QtCore.Qt.AlignHCenter) 

    def _init_matrix(self, data):
        matrix_widget = QtWidgets.QWidget()
        matrix_widget.setMinimumWidth(200)
        matrix_layout = QtWidgets.QGridLayout(matrix_widget)
        matrix_layout.setSpacing(10)
        matrix_layout.setMargin(0)

        validator = QtGui.QDoubleValidator(-100000.00, 1000000.00, 4)
        validator.setNotation(QtGui.QDoubleValidator.StandardNotation)
        for i in range(16):
            item = QtWidgets.QLabel()
            #item.setValidator(validator)
            item.setText(str(round(data[i], 4)))
            item.setAlignment(QtCore.Qt.AlignHCenter) 
            item.setEnabled(False)
            matrix_layout.addWidget(item, i/4, i%4)

        return matrix_widget

    def _init_samples_values(self, names, data, isEditable):
        values = QtWidgets.QTableWidget()
        values.setRowCount(1)
        values.setColumnCount(len(names))
        values.setHorizontalHeaderLabels(names)

        for i, value in enumerate(data):
            item = QtWidgets.QTableWidgetItem()
            item.setText(str(round(value, 4)))
            if not isEditable:
                item.setFlags(QtCore.Qt.ItemIsEditable)
            values.setItem(0, i, item)   

        self._tables.append(values)
        return values

    def populate_samples(self, data):
        # Delete old widgets
        for sample in self._samples + self._separators:
            self._samples_layout.removeWidget(sample)
            sample.deleteLater()

        # Delete add button    
        self._add_sample.deleteLater()

        # Reset lists
        self._samples = []
        self._separators = []
        self._tables = []
        self._delete_buttons = []
        self._selectp_buttons = []
        self._selectv_buttons = []

        # Looping over samples amount
        for i in range(data["sa"]):

            # Add separators only if it not the first sample
            if int(i) != 0:
                h1 = SamplesWidget.HLine()
                self._samples_layout.addWidget(h1)
                self._separators.append(h1)

            # Create a widget container for each sample
            single_sample = QtWidgets.QWidget()
            single_sample.setMinimumHeight(170)     
            single_sample.setMaximumHeight(170)   

            # Create a layout
            single_sample_layout = QtWidgets.QGridLayout(single_sample)  

            # Create and add a label for the position
            pos_label = QtWidgets.QLabel(data["sp"]["strings"][i])
            single_sample_layout.addWidget(pos_label, 0, 0, 1, 1)

            # Create and add a widget to show the matrix
            # of the sample position
            pos_widget = self._init_matrix(data["sp"]["floats"][i])
            pos_widget.setMaximumWidth(self.width()/4)
            single_sample_layout.addWidget(pos_widget, 1, 0, 1, 1)

            # Create and add vertical separator
            v1 = SamplesWidget.VLine()
            single_sample_layout.addWidget(v1, 0, 1, 2, 1)

            # Create and add a label for the value
            value_label = QtWidgets.QLabel(data["sv"]["attributes"][i])
            single_sample_layout.addWidget(value_label, 0, 2, 1, 1)

            # Create and add a widget to show and edit
            # the vaue of the current sample 
            samples_values = self._init_samples_values(data["sv"]["strings"][i], 
                                                       data["sv"]["floats"][i], 
                                                       data["isSampleEditable"])
            single_sample_layout.addWidget(samples_values, 1, 2, 1, 1)

            # Create a widget and a layout for the per-sample buttons
            samples_buttons = QtWidgets.QWidget()
            samples_buttons_layout = QtWidgets.QHBoxLayout(samples_buttons)

            # Create and add delete button
            delete_sample = QtWidgets.QPushButton(const.B_DELETE)
            delete_sample.setObjectName(const.CSS_RED)
            samples_buttons_layout.addWidget(delete_sample)
            self._delete_buttons.append(delete_sample)

            # Create and add select sample position button
            select_sample_pos = QtWidgets.QPushButton(const.B_SSP)
            samples_buttons_layout.addWidget(select_sample_pos)
            self._selectp_buttons.append(select_sample_pos)

            # Create and add select sample value button
            select_sample_value = QtWidgets.QPushButton(const.B_SSV)
            samples_buttons_layout.addWidget(select_sample_value)
            self._selectv_buttons.append(select_sample_value)

            # Add buttons widget to sample container layout
            single_sample_layout.addWidget(samples_buttons, 2, 0, 1, 3)

            # Add sample container to main layout
            self._samples_layout.addWidget(single_sample)
            self._samples.append(single_sample)

        # Add "Add" button to main layout
        self._add_sample = QtWidgets.QPushButton(const.B_ADD)
        self._add_sample.setMaximumWidth(200)
        self._samples_layout.addWidget(self._add_sample)
        self._samples_layout.setAlignment(self._add_sample, QtCore.Qt.AlignHCenter) 

        # Make connections
        self._make_connections()

    def _make_connections(self):
        for table in self._tables:
            table.itemChanged.connect(self.emit_sample_cell_updated_signal)

        self._add_sample.clicked.connect(self.emit_add_sample_pressed_signal)

        for db, sp, sv in zip(self._delete_buttons, self._selectp_buttons, self._selectv_buttons):
            db.clicked.connect(lambda i=self._delete_buttons.index(db): self.emit_delete_sample(i))
            sp.clicked.connect(lambda i=self._selectp_buttons.index(sp): self.emit_selectp_sample(i))
            sv.clicked.connect(lambda i=self._selectv_buttons.index(sv): self.emit_selectv_sample(i))

    def emit_sample_cell_updated_signal(self):
        values = []
        for table in self._tables:
            row = []
            for i in range(table.columnCount()):
                row.append(float(str(table.item(0, i).text())))
            values.append(row)

        self.sample_cell_updated_signal.emit(values)

    def emit_add_sample_pressed_signal(self):
        self.add_sample_pressed_signal.emit()

    def emit_delete_sample(self, index):
        self.delete_sample_pressed_signal.emit(index)

    def emit_selectp_sample(self, index):
        self.select_samplep_pressed_signal.emit(index)

    def emit_selectv_sample(self, index):
        self.select_samplev_pressed_signal.emit(index)

    def reset(self):

        all_widgets = [self._samples, self._separators, self._tables,
                       self._delete_buttons, self._selectp_buttons, self._selectv_buttons]

        for widget_list in all_widgets:
            for w in widget_list:
                w.deleteLater()

        self._samples = []
        self._separators = []
        self._tables = []
        self._delete_buttons = []
        self._selectp_buttons = []
        self._selectv_buttons = []