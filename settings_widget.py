# -*- coding: utf-8 -*-
from maya import cmds
from Qt import QtWidgets, QtCore, QtGui
from ddRbfUi import const
from ddRbfUi import base_widget
import ddRbfUi.CSS as CSS

class SettingsWidget(base_widget.BaseWidget):
    update_signal = QtCore.Signal(list)

    def __init__(self, parent=None):
        super(SettingsWidget, self).__init__(parent=parent)

    def _init_interface(self):
        """Initialize Interface"""
        
        super(SettingsWidget, self)._init_interface()

        # Create main layout
        settings_layout = QtWidgets.QGridLayout(self)
        settings_layout.setSpacing(1)
        settings_layout.setMargin(1)
        settings_layout.setAlignment(QtCore.Qt.AlignTop)

        # Create and add the name row
        name_label = QtWidgets.QLabel(const.L_NAME)
        self._name_line = QtWidgets.QLineEdit()
        self._name_line.setEnabled(False)

        settings_layout.addWidget(name_label, 0, 0)
        settings_layout.addWidget(self._name_line, 0, 1)

        # Create and add the driver row
        driver_label = QtWidgets.QLabel(const.L_DRIVER)
        self._driver_line = QtWidgets.QLineEdit()
        self._driver_line.setEnabled(False)

        settings_layout.addWidget(driver_label, 1, 0)
        settings_layout.addWidget(self._driver_line, 1, 1)

        # Create and add the samples amount row
        samples_label = QtWidgets.QLabel(const.L_NSAMPLES)
        self._samples_line = QtWidgets.QLineEdit("0")
        self._samples_line.setEnabled(False)
        self._samples_line.setValidator(QtGui.QIntValidator(0, 1000000))

        settings_layout.addWidget(samples_label, 2, 0)
        settings_layout.addWidget(self._samples_line, 2, 1)

        # Create and add the sample size row
        samplessize_label = QtWidgets.QLabel(const.L_SSAMPLES)
        self._samplessize_line = QtWidgets.QLineEdit("0")
        self._samplessize_line.setEnabled(False)
        self._samplessize_line.setValidator(QtGui.QIntValidator(0, 1000000))

        settings_layout.addWidget(samplessize_label, 3, 0)
        settings_layout.addWidget(self._samplessize_line, 3, 1)

        # Create and add the kernel row
        interpolation_label = QtWidgets.QLabel(const.L_INTERPTYPE)
        self._interpolation_line = QtWidgets.QComboBox()

        self._interpolation_line.addItem(const.CB_GAUSS)
        self._interpolation_line.addItem(const.CB_MULTIQUADRATIC)
        self._interpolation_line.addItem(const.CB_INVERSEQUADRATIC)
        self._interpolation_line.addItem(const.CB_INVERSEMULTIQUADRATIC)
        self._interpolation_line.addItem(const.CB_POLYHARMONIC)
        self._interpolation_line.addItem(const.CB_THINPLATE)
        self._interpolation_line.setEnabled(False)

        """
        d = distance in R^n; r = d^2, k=round(ε)
        Gaussian:              e^(-ε*r)
        MultiQuadratic:        (1+(ε*d)^2)^1/2
        InverseQuadratic:      1/(ε*d)^2
        InverseMultiQuadratic: 1/(1+(ε*d)^2)^1/2
                               d^k         with k=1,3,5,...,2n+1
        PolyHarmonic:        {                                  }
                               log(d)*d^k  with k=2,4,6,...,2n
        ThinPlate:             log(d)*d^2
        """
        #interpolation_line.setValidator(QtGui.QDoubleValidator(0, 1000000))

        settings_layout.addWidget(interpolation_label, 4, 0)
        settings_layout.addWidget(self._interpolation_line, 4, 1)

        # Create and add the epsilon row
        epsilon_label = QtWidgets.QLabel(const.L_EPSILON)
        self._epsilon_line = QtWidgets.QLineEdit("0")
        self._epsilon_line.setValidator(QtGui.QDoubleValidator(0, 1000000, 2))
        self._epsilon_line.setEnabled(False)

        settings_layout.addWidget(epsilon_label, 5, 0)
        settings_layout.addWidget(self._epsilon_line, 5, 1)

        # Create and add the Update button
        self._update_button = QtWidgets.QPushButton(const.B_UPDATE)
        self._update_button.setObjectName(const.CSS_PINK)
        settings_layout.addWidget(self._update_button, 6, 0, 1, 2)

        # Make Connections
        self._make_connections()

    def _make_connections(self):
        self._update_button.clicked.connect(self.emit_updated_settings)

    def emit_updated_settings(self):
        data = {"n": self._name_line.text(), 
                "d": self._driver_line.text(), 
                "k": self._interpolation_line.currentText(), 
                "ep": self._epsilon_line.text()}
        self.update_signal.emit(data)

    def populate_settings(self, data):
        self._name_line.setText(data["n"])
        self._driver_line.setText(data["d"][0])
        self._samples_line.setText(str(data["sa"]))
        self._samplessize_line.setText(str(data["ss"]))
        self._interpolation_line.setCurrentIndex(data["k"])
        self._epsilon_line.setText(str(data["ep"]))

        self._name_line.setEnabled(True)
        self._driver_line.setEnabled(True)
        self._interpolation_line.setEnabled(True)
        self._epsilon_line.setEnabled(True)

    def reset(self):
        self._name_line.setText("")
        self._driver_line.setText("")
        self._samples_line.setText(str(0))
        self._samplessize_line.setText(str(0))
        self._interpolation_line.setCurrentIndex(0)
        self._epsilon_line.setText(str(1))

        self._name_line.setEnabled(False)
        self._driver_line.setEnabled(False)
        self._interpolation_line.setEnabled(False)
        self._epsilon_line.setEnabled(False)

        