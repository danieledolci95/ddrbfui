# -*- coding: utf-8 -*-

from maya import cmds
from Qt import QtWidgets, QtCore, QtGui
from ddRbfUi import base_widget
from ddRbfUi import const
import ddRbfUi.CSS as CSS
from ddRbfUi import rbf_model
from ddRbfUi import settings_widget
from ddRbfUi import samples_widget
from ddRbfUi import creator_widget


class ddRbfView(base_widget.BaseWidget):
    selection_changed_signal = QtCore.Signal(str)
    select_signal = QtCore.Signal(str)
    toggle_signal = QtCore.Signal(str)
    delete_signal = QtCore.Signal(str)
    mirror_signal = QtCore.Signal(str)

    def __init__(self, parent=None):

        #Check if exists and delete it
        self.check_for_existing()

        self._rbf_model = rbf_model.RbfModel() 

        self._settings_widget = None
        self._samples_widget = None

        super(ddRbfView, self).__init__(parent=parent, 
                                        object_name=const.MAIN_OBJECT_NAME, 
                                        window_name=const.MAIN_WINDOW_NAME)
        

    def _init_interface(self):
        """
         _________________________________________________________________________________
        | File Edit Settings ?                                                            |
        |_________________________________________________________________________________|
        |  __________________                                                             |
        | | ddRbf1           |          SELECT          |                                 |  
        | | ddRbf2           |          TOGGLE          |                                 |
        | | ...              |          DELETE          |                                 |
        | | ddRbfN           |          MIRROR          |                                 |
        | |                  | _________________________|                                 |
        | |                  | Driver:        Locator   |                                 |
        | |                  | Driven:        Cube      |                                 |
        | |                  | N. Of Samples: 5         |                                 |
        | |                  | Interpolation: Gaussian  |                                 |
        | |                  | Epsilon:       1         |                                 | 
        | |                  |           UPDATE         |                                 |
        | |__________________|                          |                                 |
        |_________________________________________________________________________________|
        |                        _____________________________________________________  |^|                                         
        | 1.0  0.0  0.0  0.0  | |     |     |     |     |     |     |     |     |     | |_|
        | 0.0  1.0  0.0  0.0  | |     |  1  |  2  |  3  |  4  |  5  |  6  | ... |  n  | | |
        | 0.0  0.0  1.0  0.0  | |_____|_____|_____|_____|_____|_____|_____|_____|_____| | |
        | 0.0  0.0  0.0  1.0  | |     |     |     |     |     |     |     |     |     | | |
        |                     | |  1  | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | | |
        |       DELETE        | |_____|_____|_____|_____|_____|_____|_____|_____|_____| | |
        | _____________________________________________________________________________ | |
        |                        _____________________________________________________  | |                                         
        | 1.0  0.0  0.0  0.0  | |     |     |     |     |     |     |     |     |     | | |
        | 0.0  1.0  0.0  0.0  | |     |  1  |  2  |  3  |  4  |  5  |  6  | ... |  n  | | |
        | 0.0  0.0  1.0  0.0  | |_____|_____|_____|_____|_____|_____|_____|_____|_____| | |
        | 0.0  0.0  0.0  1.0  | |     |     |     |     |     |     |     |     |     | | |
        |                     | |  1  | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | | |
        |       DELETE        | |_____|_____|_____|_____|_____|_____|_____|_____|_____| | |
        | _____________________________________________________________________________ | |
        |                        _____________________________________________________  | |                                         
        | 1.0  0.0  0.0  0.0  | |     |     |     |     |     |     |     |     |     | |_|
        | 0.0  1.0  0.0  0.0  | |     |  1  |  2  |  3  |  4  |  5  |  6  | ... |  n  | | |
        |_______________________________________________________________________________|v|

        """
        self.setWindowFlags(QtCore.Qt.Window) 

        self.setMinimumHeight(650)
        self.setMaximumHeight(650)
        self.setMinimumWidth(800)
        self.setMaximumWidth(800)
        self.setContentsMargins(-10, -10, -10, -10)

        super(ddRbfView, self)._init_interface()
        
        # Create Main Layout
        main_layout = QtWidgets.QVBoxLayout(self)

        # Create and add the menu
        menu = self._create_menu_bar()
        main_layout.addWidget(menu)
        
        # Create and add separator
        h1 = ddRbfView.HLine()
        main_layout.addWidget(h1)

        # Create a "contnet_widget" to contain rbf list,
        # buttons, update widget and creator widget
        content_widget = QtWidgets.QWidget()
        content_widget.setMaximumHeight(self.width()/3)
        content_layout = QtWidgets.QGridLayout(content_widget)

        # Create and add the rbf list
        self._rbflist = self._create_rbf_list()
        self._rbflist.setMaximumWidth(self.width()/3)
        content_layout.addWidget(self._rbflist, 0, 1, 4, 1)

        # Create and add vertical separator
        v1 = ddRbfView.VLine()
        content_layout.addWidget(v1, 0, 1, 4, 1)

        # Create and add buttons
        rbfactions = self._create_rbf_actions()
        content_layout.addWidget(rbfactions, 0, 2, 4, 1)
        rbfactions.setMaximumWidth(self.width()/3)

        # Create and add vertical separators
        v2 = ddRbfView.VLine()
        content_layout.addWidget(v2, 0, 3, 4, 1)

        # Create and add creator widget
        self._creator_widget = creator_widget.CreatorWidget()
        content_layout.addWidget(self._creator_widget, 0, 4, 4, 1)

        # Add "content_widget" to the main layout
        main_layout.addWidget(content_widget)

        # Add horizzontal separator and add to main layout
        h2 = ddRbfView.HLine()
        main_layout.addWidget(h2)

        # Create Samples widget and add to the layout
        self._samples_scroll = self._create_samples_scroll()
        main_layout.addWidget(self._samples_scroll)

        # make connections
        self._make_connections()

    def check_for_existing(self):
        if cmds.window(const.MAIN_OBJECT_NAME, exists=True):
            cmds.deleteUI(const.MAIN_OBJECT_NAME, wnd=True)

    def _create_menu_bar(self):
        """
         _________________________________________________________________________________
        | File Edit Settings ?                                                            |
        |_________________________________________________________________________________|
        """
        menu_bar = QtWidgets.QMenuBar(self)
        file_menu = menu_bar.addMenu(const.M_FILE)
        edit_menu = menu_bar.addMenu(const.M_EDIT)
        setting_menu = menu_bar.addMenu(const.M_SETTINGS)
        help_menu = menu_bar.addMenu(const.M_HELP)
        
        return menu_bar
    def _create_rbf_list(self):
        """
        |  __________________  
        | | ddRbf1           |   
        | | ddRbf2           | 
        | | ...              | 
        | | ddRbfN           | 
        | |                  |
        | |                  | 
        | |                  | 
        | |                  | 
        | |                  | 
        | |                  |  
        | |                  | 
        | |__________________| 
        """

        rbf_list = QtWidgets.QListWidget()
        rbf_list.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        
        return rbf_list

    def _create_rbf_actions(self):
        """
                                    
        |          SELECT          |  
        |          TOGGLE          |
        |          DELETE          |
        |          MIRROR          |
        | _________________________|
        | Driver:        Locator   |
        | Driven:        Cube      |
        | N. Of Samples: 5         |
        | Interpolation: Gaussian  |
        | Epsilon:       1         | 
        |           UPDATE         |
        """
        rbf_buttons = QtWidgets.QWidget()
        rbf_buttons_layout = QtWidgets.QVBoxLayout(rbf_buttons)
        rbf_buttons_layout.setSpacing(1)
        rbf_buttons_layout.setMargin(0)
        rbf_buttons_layout.setAlignment(QtCore.Qt.AlignTop)

        self._select_button = QtWidgets.QPushButton(const.B_SELECT)
        rbf_buttons_layout.addWidget(self._select_button)

        self._toggle_button = QtWidgets.QPushButton(const.B_TOGGLE)
        rbf_buttons_layout.addWidget(self._toggle_button)

        self._delete_button = QtWidgets.QPushButton(const.B_RBFDELETE)
        rbf_buttons_layout.addWidget(self._delete_button)

        self._settings_widget = settings_widget.SettingsWidget()
        rbf_buttons_layout.addWidget(self._settings_widget)

        return rbf_buttons

    def _create_samples_scroll(self):
        """
        |_________________________________________________________________________________|
        |                        _____________________________________________________  |^|                                         
        | 1.0  0.0  0.0  0.0  | |     |     |     |     |     |     |     |     |     | |_|
        | 0.0  1.0  0.0  0.0  | |     |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  | | |
        | 0.0  0.0  1.0  0.0  | |_____|_____|_____|_____|_____|_____|_____|_____|_____| | |
        | 0.0  0.0  0.0  1.0  | |     |     |     |     |     |     |     |     |     | | |
        |                     | |  1  | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | | |
        |       DELETE        | |_____|_____|_____|_____|_____|_____|_____|_____|_____| | |
        | _____________________________________________________________________________ | |
        |                        _____________________________________________________  | |                                         
        | 1.0  0.0  0.0  0.0  | |     |     |     |     |     |     |     |     |     | | |
        | 0.0  1.0  0.0  0.0  | |     |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  | | |
        | 0.0  0.0  1.0  0.0  | |_____|_____|_____|_____|_____|_____|_____|_____|_____| | |
        | 0.0  0.0  0.0  1.0  | |     |     |     |     |     |     |     |     |     | | |
        |                     | |  1  | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | 0.0 | | |
        |       DELETE        | |_____|_____|_____|_____|_____|_____|_____|_____|_____| | |
        | _____________________________________________________________________________ | |
        |                        _____________________________________________________  | |                                         
        | 1.0  0.0  0.0  0.0  | |     |     |     |     |     |     |     |     |     | |_|
        | 0.0  1.0  0.0  0.0  | |     |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  | | |
        |_______________________________________________________________________________|v|
        """
        self._samples_widget = samples_widget.SamplesWidget()   
        self._settings_widget.setSizePolicy(QtWidgets.QSizePolicy.Expanding, 
                                          QtWidgets.QSizePolicy.Expanding) 
        
        samples_scroll = QtWidgets.QScrollArea()
        samples_scroll.setWidget(self._samples_widget )  
        samples_scroll.setWidgetResizable(True)
        samples_scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)

        return samples_scroll

    def _make_connections(self):
        self._rbflist.currentTextChanged.connect(self.emit_selection_changed)
        self._rbflist.clicked.connect(self.emit_selection_changed)

        self._select_button.clicked.connect(self.emit_select)
        self._toggle_button.clicked.connect(self.emit_toggle)
        self._delete_button.clicked.connect(self.emit_delete)

    def emit_selection_changed(self):       
        if self._rbflist.currentItem():
            print "EMITTTING"
            print self._rbflist.currentItem().text()
            self.selection_changed_signal.emit(self._rbflist.currentItem().text())

    def emit_select(self):
        if self._rbflist.currentItem():
            self.select_signal.emit(self._rbflist.currentItem().text())

    def emit_toggle(self):
        if self._rbflist.currentItem():
            self.toggle_signal.emit(self._rbflist.currentItem().text())

    def emit_delete(self):
        if self._rbflist.currentItem():
            self.delete_signal.emit(self._rbflist.currentItem().text())

    def emit_mirror(self):
        if self._rbflist.currentItem():
            self.mirror_signal.emit(self._rbflist.currentItem().text())

    def populate_rbf_list(self, the_list):
        self._rbflist.clear()
        self._settings_widget.reset()
        self._settings_widget.reset()
        for rbf in the_list:
            self._rbflist.addItem(rbf)

    def get_settings_widget(self):
        return self._settings_widget

    def get_samples_widget(self):
        return self._samples_widget 

    def get_creator_widget(self):
        return self._creator_widget 
